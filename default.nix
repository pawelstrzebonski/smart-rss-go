{ pkgs ? import <nixpkgs> {} }:
with pkgs;

buildGoModule rec {
  name = "smartrssgo";  
  nativeBuildInputs = [ pkg-config ];
  buildInputs = [
	go
    xorg.libX11
    xorg.libXcursor
    xorg.libXrandr
    xorg.libXinerama
    xorg.libXi
    xorg.libXext
    libGL
    xorg.libXxf86vm
  ];
  vendorSha256="087wli1fl6bbbpl6xq7k0mm252x1fy2fwd98i5kh04qpm5fwzsl3";
  rev = "23bafb7a0d11faa10cbb3c8caf231ef9049aa5b3";
  src = fetchFromGitLab {
    inherit rev;
    owner = "pawelstrzebonski";
    repo = "smart-rss-go";
    sha256 = "0cygwzxivi973zw840nfxq7syjhyx3bjh44hy3kl56x17fxyp300";
  };
  meta = with lib; {
    description = "A machine learning enhanced RSS feed manager, written in Go.";
    homepage = "https://gitlab.com/pawelstrzebonski/smart-rss-go";
    license = licenses.agpl3;
    #maintainers = with maintainers; [ pawelstrzebonski ];
    platforms = platforms.linux;
  };
}
