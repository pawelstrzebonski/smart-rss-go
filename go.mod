module smartrssgo

go 1.16

require (
	fyne.io/fyne/v2 v2.1.2
	github.com/pkg/browser v0.0.0-20210911075715-681adbf594b8
	gitlab.com/pawelstrzebonski/libsmartrssgo v0.0.0-20220305213820-7a8af79b2745
)
