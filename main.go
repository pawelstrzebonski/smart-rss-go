/*
 Smart-RSS (Go), a machine learning enhance RSS feed manager, writen
 in Go.
*/
package main

import (
	"flag"
	"gitlab.com/pawelstrzebonski/libsmartrssgo"
	"log"
	"os"
	"path/filepath"
)

// Start the application, setting up the database and starting the GUI
func main() {
	// Setup the filepath for the database
	homepath, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}
	apppath := filepath.Join(homepath, ".config", "smartrss")
	err = os.MkdirAll(apppath, os.ModePerm)
	if err != nil {
		log.Fatal(err)
	}
	// Create/load the database
	smartrss := libsmartrssgo.Setup(filepath.Join(apppath, "smartrssv3.db"), libsmartrssgo.ERROR)
	// Defer closing/saving the database
	defer smartrss.Close()
	// Updated feeds on application start
	smartrss.FeedsUpdate()
	smartrss.SetAutoUpdate(600)

	// Get localization from flags
	loc := flag.String("lang", "en", "short-string for localization language")
	flag.Parse()
	// Run GUI frontend
	run_gui(smartrss, loc)
}
