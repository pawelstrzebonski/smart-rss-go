# Smart-RSS Go

![Smart-RSS Icon](assets/Icon.png)

A machine learning enhanced RSS feed manager (written in Go). Unlike the parallel project [Smart-RSS Go Web](https://gitlab.com/pawelstrzebonski/smart-rss-go-web) (which has a web-server/web-UI design and is also built on the [`libsmartrssgo`](https://gitlab.com/pawelstrzebonski/libsmartrssgo)), this project provides a desktop app with native GUI.

For more information about program design/implementation, please consult the [documentation pages](https://pawelstrzebonski.gitlab.io/smart-rss-go/).

## Features

* Automatic periodic RSS feed checking
* Machine learning scoring and ordered presentation of feed items
* Local database and processing
* Native GUI

## Screenshots

![Smart-RSS Screenshot](docs/img/smartrssgo_main.png)

## Usage/Installation

This repository contains the Go source code in the top level. It can be built using the standard Go compiler and may require installation of build dependencies from your systems package respositories. A `Makefile` is included with most of the relevant build/install commands, although build dependency installation is operating system dependent and is not included.

If you have [Nix](https://nixos.org/) then you can use the included `shell.nix` to start a shell with installed dependencies for development, or else you can use the `default.nix` script to build/install a specific version of this application. Note that `default.nix` specifies the commit to be built, so it may need to be updated to build the latest version.
