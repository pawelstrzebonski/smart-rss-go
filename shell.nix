with (import <nixpkgs> {});

mkShell rec {
  buildInputs = [
    go
    pkg-config
    xorg.libX11
    xorg.libXcursor
    xorg.libXrandr
    xorg.libXinerama
    xorg.libXi
    xorg.libXext
    libGL
    xorg.libXxf86vm
  ];
}
