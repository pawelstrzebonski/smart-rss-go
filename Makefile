## ============== Smart-RSS Go Makefile ===============
## ----------------------------------------------------
## This Makefile should support building using stardard
## Go toolset, as well as the Nix toolset.
## ----------------------------------------------------

help:			## Show this help
	@sed -ne '/@sed/!s/## //p' Makefile
	
localizations:		## Build localizations
	#go-localize -input localizations_src -output localizations
	go generate

build: go.sum		## Build application using Go toolset
	go build

install: smartrssgo	## Install application using Go toolset
	go install

smartrssgo: go.sum	## Build application using Go toolset
	go build

go.sum:			## Setup Go dependencies packages using Go toolset
	go mod tidy

build-nix:		## Build using Nix toolset
	nix-build

install-nix: result	## Install using Nix toolset
	nix-env -i ./result

result:			## Build using Nix toolset
	nix-build

fyne:			## Install Fyne tools
	go get fyne.io/fyne/v2/cmd/fyne

android:		## Build Android app
	fyne package -os android -appID 'com.gitlab.pawelstrzebonski.smartrss' -icon assets/Icon.png

Linux:			## Package Linux app
	fyne package -os linux

build-docs:		## Build documentation pages
	mkdocs build

public:			## Build documentation pages
	mkdocs build

serve-docs: public	## Serve documentation pages (locally)
	mkdocs serve

clean:			## Clean up directory by deleting files
	rm -f smartrssgo
	rm -f result
	rm -f go.sum
	rm -rf public/
