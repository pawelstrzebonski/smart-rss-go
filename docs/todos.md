# TODOs

This project is by no means finished/polished. A the following is a shopping list of improvements (in no particular order):

* Better GUI:
	* expose more application settings
	* icons?
	* count # of unread items
	* vertical scrolling/resizing
	* nicer presentation
* Better handling of non-ASCII/international content
* More/better GUI string translations
* Test/verify whether Android/mobile app will build
* General code/project improvements:
	* tests
	* CI
	* build documentation from source
