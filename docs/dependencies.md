# Dependencies

This application makes use of the following Go packages:

* [`libsmartrssgo`](https://gitlab.com/pawelstrzebonski/libsmartrssgo): Back-end for RSS/database/machine-learning components
* [`browser`](https://github.com/pkg/browser): Opening links from this applications in the user's default browser
* [`fyne`](https://github.com/fyne-io/fyne): GUI interface
* [`go-localize`](https://github.com/m1/go-localize): Not a build code dependency, but rather a tool to generate localization Go source code
