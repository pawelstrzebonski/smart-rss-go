# Smart-RSS Go

Smart-RSS Go is a machine learning enhanced RSS feed manager, written in Go (as opposed to the original [Python version](https://gitlab.com/pawelstrzebonski/smart-rss) or the subsequent [Rust version](https://gitlab.com/pawelstrzebonski/rusty-smart-rss) practice projects) and with a desktop GUI. Built upon the [`libsmartrssgo`](https://gitlab.com/pawelstrzebonski/libsmartrssgo) library, it is a parallel project to the [`Smart-RSS Go Web`](https://gitlab.com/pawelstrzebonski/smart-rss-go-web) application that provides a web-server/web-browser interface instead.

## Features

* Automatic periodic RSS feed checking
* Machine learning scoring and ordered presentation of feed items
* Local database and processing
* Native GUI

## Screenshots

![Add-new-feed window](img/smartrssgo_addfeed.png)

Window for adding a new RSS feed

![Main window](img/smartrssgo_main.png)

Main application window

![Remove-feed window](img/smartrssgo_removefeed.png)

Window for removing a RSS feed
