package main

//go:generate go-localize -input localizations_src -output localizations

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/widget"
	"github.com/pkg/browser"
	"gitlab.com/pawelstrzebonski/libsmartrssgo"
	"log"
	"smartrssgo/localizations"
	"strconv"
)

func MinInt(a int, b int) int {
	if a < b {
		return a
	} else {
		return b
	}
}

// Open a window for adding a new RSS feed
func add_feed_window(a fyne.App, smartrss libsmartrssgo.SmartRSSBackend, l *localizations.Localizer) {
	// Entry for feed URL
	input := widget.NewEntry()
	input.SetPlaceHolder(l.Get("messages.feed_url") + "...")
	// Entry for update period, with validator checking if it is a (positive) integer
	input2 := widget.NewEntry()
	input2.SetPlaceHolder("3600")
	input2.Validator = func(s string) error {
		_, e := strconv.ParseUint(s, 10, 64)
		return e
	}
	w2 := a.NewWindow(l.Get("messages.add_feed"))
	w2.SetContent(
		container.NewVBox(
			widget.NewLabel(l.Get("messages.feed_url")+":"),
			input,
			widget.NewLabel(l.Get("messages.feed_update_interval")+":"),
			input2,
			container.NewHBox(
				widget.NewButton(l.Get("messages.add_feed"), func() {
					i, _ := strconv.ParseUint(input2.Text, 10, 64)
					smartrss.FeedAdd(input.Text, i)
					input.Text = ""
					w2.Close()
				}),
				widget.NewButton(l.Get("messages.exit"), func() {
					w2.Close()
				}),
			)))
	w2.Show()
}

// Open a window for removing RSS feeds
func remove_feed_window(a fyne.App, smartrss libsmartrssgo.SmartRSSBackend, l *localizations.Localizer) {
	// Create list of existing feeds
	feeds := smartrss.FeedsList()
	urls := make([]string, len(feeds))
	for i, feed := range feeds {
		urls[i] = feed.Url
	}
	// URL to be removed
	url := ""
	// Entry for feed URL
	input := widget.NewSelect(urls, func(x string) { url = x })
	w2 := a.NewWindow(l.Get("messages.remove_feed"))
	w2.SetContent(
		container.NewVBox(
			widget.NewLabel(l.Get("messages.feed_to_remove")),
			input,
			container.NewHBox(
				widget.NewButton(l.Get("messages.remove_feed"), func() {
					if smartrss.FeedRemove(url) {
						w2.Close()
					}
				}),
				widget.NewButton(l.Get("messages.exit"), func() {
					w2.Close()
				}),
			)))
	w2.Show()
}

// Run the GUI
func run_gui(smartrss libsmartrssgo.SmartRSSBackend, loc *string) {
	// Setup localizations
	l := localizations.New(*loc, "en")

	NUMITEMSPERPAGE := 4
	items := smartrss.ItemsUnreadGetLimited(NUMITEMSPERPAGE)
	log.Print(len(items), " unread items")
	if len(items) > 0 {
		log.Print("Item scores ranging from ", items[len(items)-1].Score, " to ", items[0].Score)
	}

	a := app.New()
	w := a.NewWindow("Smart-RSS")

	itemwidgets := make([]*fyne.Container, NUMITEMSPERPAGE)
	itemtitles := make([]binding.String, NUMITEMSPERPAGE)
	itemsummaries := make([]binding.String, NUMITEMSPERPAGE)
	itemscores := make([]binding.String, NUMITEMSPERPAGE)
	for i0 := 0; i0 < NUMITEMSPERPAGE; i0++ {
		i := i0
		itemtitles[i] = binding.NewString()
		itemsummaries[i] = binding.NewString()
		itemscores[i] = binding.NewString()
		titlelabel := widget.NewLabelWithData(itemtitles[i])
		summarylabel := widget.NewLabelWithData(itemsummaries[i])
		scorelabel := widget.NewLabelWithData(itemscores[i])
		itemwidgets[i] = container.NewVBox(
			container.NewHScroll(titlelabel),
			container.NewHScroll(summarylabel),
			container.NewHBox(
				scorelabel,
				widget.NewButton(l.Get("messages.open"), func() {
					browser.OpenURL(items[i].Url)
					smartrss.ItemMarkOpened(items[i].Url)
				}),
				widget.NewButton(l.Get("messages.like"), func() {
					smartrss.ItemLike(items[i].Url)
				}),
				widget.NewButton(l.Get("messages.dislike"), func() {
					smartrss.ItemDislike(items[i].Url)
				}),
			))
	}
	update_item_widgets := func(items []libsmartrssgo.MyItem) {
		for i, _ := range itemwidgets {
			if len(items) > i {
				itemtitles[i].Set(items[i].Title)
				itemsummaries[i].Set(items[i].Summary)
				itemscores[i].Set(l.Get("messages.score") + fmt.Sprintf(": %f", items[i].Score))
				itemwidgets[i].Refresh()
				itemwidgets[i].Show()
			} else {
				itemwidgets[i].Hide()
			}
		}
	}
	update_item_widgets(items)

	maincontent := container.NewVBox(container.NewHBox(
		widget.NewButton(l.Get("messages.update_feeds"), func() {
			smartrss.FeedsUpdate()
			items = smartrss.ItemsUnreadGetLimited(NUMITEMSPERPAGE)
			log.Print(len(items), " unread items")
			if len(items) > 0 {
				log.Print("Item scores ranging from ", items[len(items)-1].Score, " to ", items[0].Score)
			}
			update_item_widgets(items)
		}),
		widget.NewButton(l.Get("messages.next_item"), func() {
			n := MinInt(NUMITEMSPERPAGE, len(items))
			for i := 0; i < n; i++ {
				smartrss.ItemMarkViewed(items[i].Url)
			}
			items = smartrss.ItemsUnreadGetLimited(NUMITEMSPERPAGE)
			update_item_widgets(items)
		}),
	))
	for _, iw := range itemwidgets {
		maincontent.Add(iw)
	}
	maincontent.Add(container.NewHBox(
		widget.NewButton(l.Get("messages.add_feed"), func() {
			add_feed_window(a, smartrss, l)
		}),
		widget.NewButton(l.Get("messages.remove_feed"), func() {
			remove_feed_window(a, smartrss, l)
		}),
		widget.NewButton(l.Get("messages.exit"), func() {
			w.Close()
		}),
	))
	w.SetContent(maincontent)

	w.Show()
	a.Run()
}
